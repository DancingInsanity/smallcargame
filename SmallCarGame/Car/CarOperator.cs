﻿using SmallCarGame.Core;
using SmallCarGame.Input;
using System;

namespace SmallCarGame
{
	class CarOperator: Initializable, IDisposable, CarProvider
	{
		private const int _lineIndex = 10;
		private const ConsoleKey _leftKey = ConsoleKey.LeftArrow;
		private const ConsoleKey _rightKey = ConsoleKey.RightArrow;

		private readonly Field _field;
		private readonly InputProvider _inputProvider;
		private readonly Loopable _loopable;

		private int _index;
		private int _formerIndex;
		private ConsoleKey _formerKey = default(ConsoleKey);
		private Car _car;
		
		public FieldUnit CarUnit { get { return _field.Lines[_lineIndex].Units[_index]; } }
		public FieldUnit FormerCarUnit { get { return _field.Lines[_lineIndex].Units[_formerIndex]; } }

		public CarOperator(Field field,
			InputProvider inputProvier,
			Loopable loopable)
		{
			_field = field;
			_inputProvider = inputProvier;
			_loopable = loopable;
			createCar();
		}

		public void Initialize()
		{
			_index = _field.Lines[_lineIndex].Length / 2;
			_formerIndex = _index;
			if (!CarUnit.IsAccessable)
				throw new Exception("Start Field unit of the car is not accessable!");
			CarUnit.IsPlayerUnit = true;
			_loopable.AddLoopAction(LoopContext.Loop, checkUserInput, 900);
			updateCar();
		}


		private void updateCar()
		{
			_car.CurrentFieldUnit = CarUnit;
			_car.FormerFieldUnit = FormerCarUnit;
		}

		private void checkUserInput()
		{
			UserInput currentInput = _inputProvider.GetInput();
			if (_formerKey == currentInput.KeyInfo.Key)
				return;
			checkForCarKeyInput(currentInput);
			_formerKey = currentInput.KeyInfo.Key;
		}

		private void checkForCarKeyInput(UserInput currentInput)
		{
			switch(currentInput.KeyInfo.Key)
			{
				case _leftKey:
					moveCar(-1);
					break;
				case _rightKey:
					moveCar(1);
					break;
				default:
					return;
			}
		}

		private void moveCar(int delta)
		{
			int newIndex = _index + delta;
			if (newIndex < 0 || newIndex >= _field.Lines[_lineIndex].Length)
				return;
			_formerIndex = _index;
			_index = newIndex;
			updateCar();
			setCarPos();
		}

		private void setCarPos()
		{
			_car.FormerFieldUnit.IsPlayerUnit = false;
			_car.CurrentFieldUnit.IsPlayerUnit = true;
		}


		public void Dispose()
		{
			_loopable.AddLoopAction(LoopContext.Loop, checkUserInput);
		}

		public Car GetCar()
		{
			return _car;
		}

		private void createCar()
		{
			_car = new Car();
		}
	}
}
