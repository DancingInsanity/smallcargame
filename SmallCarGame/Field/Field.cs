﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallCarGame
{
	class Field
	{
		public List<FieldLine> Lines { get; }
		public int Height { get { return Lines.Count; } }
		public int Width { get { return Lines.Aggregate((u1, u2) => u1.Length > u2.Length ? u1 : u2).Length; } }


		public Field(List<FieldLine> lines)
		{
			Lines = lines;
		}

		public void UpdateFieldLine(int index, FieldLine line)
		{
			FieldLine fieldLine = Lines[index];
			updateFieldLength(fieldLine, line.Length);
			for (int i = 0; i < line.Length; i++)
				fieldLine.UpdateFieldUnit(i, line.Units[i]);
		}

		private void updateFieldLength(FieldLine line, int targetLength)
		{
			for (int i = line.Length; i < targetLength; i++)
				line.AddUnit(new FieldUnit());

			int formerLength = line.Length;
			for (int i = targetLength; i < formerLength; i++)
				line.RemoveLastUnit();
		}
	}
}
