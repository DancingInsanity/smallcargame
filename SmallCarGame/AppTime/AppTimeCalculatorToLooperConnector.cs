﻿using SmallCarGame.Core;
using SmallCarGame.Time;
using System;

namespace SmallCarGame
{
	public class AppTimeCalculatorToLooperConnector : Initializable, IDisposable
	{
		private Loopable _loopable;
		private AppTimeCalculator _appTimeCalculator;

		public AppTimeCalculatorToLooperConnector(Loopable loopable,
			AppTimeCalculator appTimeCalculator)
		{
			_loopable = loopable;
			_appTimeCalculator = appTimeCalculator;
		}

		public void Initialize()
		{
			_loopable.AddLoopAction(LoopContext.Loop, _appTimeCalculator.Calculate, 10000);
		}

		public void Dispose()
		{
			_loopable.RemoveLoopAction(LoopContext.Loop, _appTimeCalculator.Calculate);
		}
	}
}