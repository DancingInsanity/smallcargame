﻿using SmallCarGame.Collections;
using SmallCarGame.Time;
using System;

namespace SmallCarGame.Core
{
    public class Looper: Initializable, Loopable
    {
        private readonly int _fixedLoopsPerSecond;
        private readonly AppTimeProvider _appTimeProvider;

        private double _fixedLoopDuration = 0;
        private bool _isRunning = false;
		private double _leftFixedDelta = 0;


		private PrioritizedList<Action> _loopActions = new PrioritizedList<Action>();
		private PrioritizedList<Action> _fixedLoopActions = new PrioritizedList<Action>();


        public Looper(AppTimeProvider appTimeProvider,
            int fixedLoopsPerSecond = 60)
        {
            _appTimeProvider = appTimeProvider;
            _fixedLoopsPerSecond = fixedLoopsPerSecond;
        }

		public void Initialize()
		{
			_fixedLoopDuration = 1.0 / _fixedLoopsPerSecond;
        }

		public void AddLoopAction(LoopContext context, Action action, int priority = 0)
		{
			PrioritizedList<Action> list = getListByAction(context);
			list.Add(priority, action);
		}

		public void RemoveLoopAction(LoopContext context, Action action)
		{
			PrioritizedList<Action> list = getListByAction(context);
			list.Remove(action);
		}

		private PrioritizedList<Action> getListByAction(LoopContext context)
		{
			switch(context)
			{
				case LoopContext.Loop:
					return _loopActions;
				case LoopContext.FixedLoop:
					return _fixedLoopActions;
				default:
					throw new NotImplementedException($"The context {context} has not been implemented in {nameof(getListByAction)}");
			}
		}

        public void StartLoop()
        {
            _isRunning = true;
            while (_isRunning)
                loop();
        }

        public void StopLoop()
        {
            _isRunning = false;
        }

        private void loop()
        {
			foreach (PrioritizedList<Action>.ListElement element in _loopActions.List)
				element.Value.Invoke();
			doFixedLoops();
		}

		private void doFixedLoops()
		{
			Time.AppTime time = _appTimeProvider.Get();
			double delta = _leftFixedDelta + time.FrameDurationInSeconds;
			_leftFixedDelta = delta % _fixedLoopDuration;
			int fixedLoopCallTimes = (int)(delta / _fixedLoopDuration);
			for (int i = 0; i < fixedLoopCallTimes; i++)
				fixedLoop();
		}

        private void fixedLoop()
        {
			foreach (PrioritizedList<Action>.ListElement element in _fixedLoopActions.List.ToArray())
				element.Value.Invoke();
		}
    }
}
