﻿using SmallCarGame.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallCarGame
{
	class GameOverChecker: Initializable, IDisposable
	{
		private Game _game;
		private CarProvider _carProvider;
		private Loopable _loopable;

		public GameOverChecker(Game game,
			CarProvider carProvider,
			Loopable loopable)
		{
			_game = game;
			_carProvider = carProvider;
			_loopable = loopable;
		}

		public void Initialize()
		{
			_loopable.AddLoopAction(LoopContext.Loop, checkGameOver, -1000);
		}

		public void Dispose()
		{
			_loopable.RemoveLoopAction(LoopContext.Loop, checkGameOver);
		}

		private void checkGameOver()
		{
			FieldUnit currentCarUnit = _carProvider.GetCar().CurrentFieldUnit;
			if (currentCarUnit.IsAccessable)
				return;
			_game.IsLost = true;
		}
	}
}
