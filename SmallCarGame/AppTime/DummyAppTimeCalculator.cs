﻿namespace SmallCarGame.Time
{
	public class DummyAppTimeCalculator : AppTimeCalculator
	{
		public static readonly double TimeDifference = 0.1f;

		protected override void calculateFrameDuration()
		{
			
		}

		protected override double getFrameDuration()
		{
			return TimeDifference;
		}
	}
}