﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallCarGame
{
	class Game
	{
		public int Points { get; set; } = 0;
		public bool IsLost { get; set; } = false;
	}
}
