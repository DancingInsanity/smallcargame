﻿namespace SmallCarGame.Time
{
	public abstract class AppTimeCalculator : AppTimeProvider
	{
		// The current app time
		protected AppTime _currentAppTime;

		public AppTimeCalculator()
		{
			_currentAppTime = new AppTime(0);
		}

		public AppTime Get()
		{
			return _currentAppTime;
		}

		public void Calculate()
		{
			calculateFrameDuration();
			createAppTime();
		}

		private void createAppTime()
		{
			_currentAppTime = new AppTime(getFrameDuration());
		}

		protected abstract void calculateFrameDuration();
		protected abstract double getFrameDuration();
	}
}