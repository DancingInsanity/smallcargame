﻿using System;
using SmallCarGame.Input;
using SmallCarGame.Core;

namespace SmallCarGame
{
	class InputLogger: Initializable, IDisposable
	{
		private InputProvider _provider;
		private Loopable _loopable;

		public InputLogger(InputProvider provider,
			Loopable loopable)
		{
			_provider = provider;
			_loopable = loopable;
		}

		public void Initialize()
		{
			_loopable.AddLoopAction(LoopContext.Loop, logKeyInput);
		}

		public void Dispose()
		{
			_loopable.RemoveLoopAction(LoopContext.Loop, logKeyInput);
		}

		private void logKeyInput()
		{
			UserInput input = _provider.GetInput();
			if (input.KeyInfo.Key == default(ConsoleKey))
				return;
			Console.Clear();
			Console.WriteLine($"The User pressed the '{input.KeyInfo.Key}' key");
		}
	}
}
