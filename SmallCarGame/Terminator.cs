﻿using SmallCarGame.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallCarGame.Core
{
	class Terminator: Initializable, IDisposable
	{
		private const int _loopPriority = 0;

		private readonly Looper _looper;
		private readonly ConsoleKey[] _terminationKeys;
		private readonly InputProvider _inputProvider;

		public Terminator(Looper looper,
			InputProvider inputProvider,
			ConsoleKey[] terminationKeys)
		{
			_looper = looper;
			_terminationKeys = terminationKeys;
			_inputProvider = inputProvider;
		}

		public void Initialize()
		{
			_looper.AddLoopAction(LoopContext.Loop, checkTermination, _loopPriority);
		}

		public void Dispose()
		{
			_looper.RemoveLoopAction(LoopContext.Loop, checkTermination);
		}

		private void checkTermination()
		{
			UserInput input = _inputProvider.GetInput();
			if (!_terminationKeys.Contains(input.KeyInfo.Key))
				return;
			_looper.StopLoop();
			System.Environment.Exit(0);
		}
	}
}
