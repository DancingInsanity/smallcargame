﻿namespace SmallCarGame.Time
{
	public interface AppTimeProvider
    {
        AppTime Get();
    }
}
