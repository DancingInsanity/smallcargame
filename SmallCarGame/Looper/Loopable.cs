﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallCarGame.Core
{
	public interface Loopable
	{
		void AddLoopAction(LoopContext context, Action action, int priority = 0);
		void RemoveLoopAction(LoopContext context, Action action);
	}
}
