﻿using SmallCarGame.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallCarGame.Input
{
	class InputProcessor : InputProvider, Initializable, IDisposable
	{
		private const int _loopPriority = 9999;
		private readonly Loopable _loopable;

		private UserInput _currentInput;


		public InputProcessor (Loopable loopable)
		{
			_loopable = loopable;
			_currentInput = new UserInput();
		}


		public void Initialize()
		{
			_loopable.AddLoopAction(LoopContext.Loop, onLoop, _loopPriority);
		}

		public void Dispose()
		{
			_loopable.RemoveLoopAction(LoopContext.Loop, onLoop);
		}


		private void onLoop()
		{
			processInput();
		}

		private void processInput()
		{
			if (!Console.KeyAvailable)
			{
				_currentInput = new UserInput(new ConsoleKeyInfo(default(char), default(ConsoleKey), false, false, false));
				return;
			}
			ConsoleKeyInfo info = Console.ReadKey(false);
			_currentInput = new UserInput(info);
		}


		public UserInput GetInput()
		{
			return _currentInput;
		}
	}
}
