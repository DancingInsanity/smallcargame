﻿using System;

namespace SmallCarGame.Input
{
	struct UserInput
	{
		public ConsoleKeyInfo KeyInfo { get; }

		public UserInput(ConsoleKeyInfo keyInfo)                                                
		{
			KeyInfo = keyInfo;
		}
	}
}
