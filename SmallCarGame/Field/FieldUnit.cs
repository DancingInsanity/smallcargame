﻿using System;

namespace SmallCarGame
{
	class FieldUnit
	{
		public bool IsAccessable
		{
			get { return _accessable; }
			set
			{
				_accessable = value;
				OnAccessableChanged?.Invoke();
			}
		}
		private bool _accessable = false;
		public event Action OnAccessableChanged;


		public bool IsPlayerUnit { get; set; } = false;


		public FieldUnit()
		{

		}

		public FieldUnit(bool accessable,
			bool isPlayerUnit)
		{
			IsAccessable = accessable;
			IsPlayerUnit = isPlayerUnit;
		}

		public FieldUnit(FieldUnit copy)
		{
			IsAccessable = copy.IsAccessable;
			IsPlayerUnit = copy.IsPlayerUnit;
		}
	}
}
