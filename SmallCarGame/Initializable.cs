﻿namespace SmallCarGame
{
    interface Initializable
    {
        void Initialize();
    }
}
