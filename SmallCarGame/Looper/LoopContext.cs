﻿namespace SmallCarGame.Core
{
	public enum LoopContext
	{
		None = -1,
		Loop = 0,
		FixedLoop = 1
	}
}
