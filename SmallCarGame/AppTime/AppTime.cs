﻿namespace SmallCarGame.Time
{
	public struct AppTime
	{
		public double FrameDurationInSeconds { get; }

		public AppTime(double deltaTimeInSeconds)
		{
			FrameDurationInSeconds = deltaTimeInSeconds;
		}

		public override bool Equals(object obj)
		{
			AppTime other = (AppTime)obj;
			return FrameDurationInSeconds == other.FrameDurationInSeconds;
		}

		public override int GetHashCode()
		{
			return 420344772 + FrameDurationInSeconds.GetHashCode();
		}

		public override string ToString()
		{
			return $"AppTime( DeltaTimeInSeconds: {FrameDurationInSeconds} )";
		}
	}
}