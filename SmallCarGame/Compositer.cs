﻿using SmallCarGame.Core;
using SmallCarGame.Input;
using SmallCarGame.Time;
using System.Collections.Generic;

namespace SmallCarGame
{
    class Compositer
    {
        static void Main(string[] args)
        {
			Game game = new Game();
			AppTimeCalculator appTimeCalculator = new AppTimeCalculatorImpl();
			Looper looper = new Looper(appTimeCalculator, 10);
			AppTimeCalculatorToLooperConnector connector = new AppTimeCalculatorToLooperConnector(looper, appTimeCalculator);
			InputProcessor inputProcessor = new InputProcessor(looper);
			Terminator terminator = new Terminator(looper, inputProcessor, new System.ConsoleKey[] { System.ConsoleKey.Escape });
			//InputLogger logger = new InputLogger(inputProcessor, looper);
			FieldFactory fieldFactory = new FieldFactory();
			Field field = fieldFactory.Create(RouteSections.StartSection);
			GraphicsIllustrator drawer = new GraphicsIllustrator(field, looper, game);
			Field[] sections = getSections(fieldFactory);
			RouteAdvancer advancer = new RouteAdvancer(field, looper, sections, appTimeCalculator);
			CarOperator carOperator = new CarOperator(field, inputProcessor, looper);
			PointsAdder pointsAdder = new PointsAdder(game, advancer);
			GameOverChecker gameOverChecker = new GameOverChecker(game, carOperator, looper);

			connector.Initialize();
			looper.Initialize();
			inputProcessor.Initialize();
			terminator.Initialize();
			//logger.Initialize();
			drawer.Initialize();
			advancer.Initialize();
			carOperator.Initialize();
			pointsAdder.Initialize();
			gameOverChecker.Initialize();

			looper.StartLoop();
		}

		private static Field[] getSections(FieldFactory fieldFactory)
		{
			Field narrowSection = fieldFactory.Create(RouteSections.NarrowSection);
			Field rightSection = fieldFactory.Create(RouteSections.RightSection);
			Field narrowRightSection = fieldFactory.Create(RouteSections.NarrowRightSection);
			Field barrierSection = fieldFactory.Create(RouteSections.BarrierSection);
			return new Field[] { narrowSection, rightSection, narrowRightSection, barrierSection };
		}
    }
}
