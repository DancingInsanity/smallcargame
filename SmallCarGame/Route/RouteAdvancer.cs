﻿using SmallCarGame.Core;
using SmallCarGame.Time;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallCarGame
{
	class RouteAdvancer: Initializable, IDisposable
	{
		private const double _startSpeed = 1;
		private const double _speedIncreasePerTimeUnit = 0.2;
		private const double _timeUnitDuration = 2;

		private readonly Field _field;
		private readonly Loopable _loopable;
		private readonly Field[] _possibleSections;
		private readonly AppTimeProvider _timeProvider;

		private Field _nextRouteSection = null;
		private int _routeSectionLineIndex = 0;
		private double _deltaTime = 0;
		private double _deltaTimeSpeedIncrease = 0;
		private int _timeUnits = 0;

		public event Action OnAdvance;

		/// <summary>
		/// Lines advanced per second
		/// </summary>
		public double Speed { get { return _startSpeed + _timeUnits * _speedIncreasePerTimeUnit; } }

		public RouteAdvancer(Field field,
			Loopable loopable,
			Field[] possibleSections,
			AppTimeProvider timeProvider)
		{
			_field = field;
			_loopable = loopable;
			_possibleSections = possibleSections;
			_timeProvider = timeProvider;
		}

		public void Initialize()
		{
			chooseNextRootSection();
			_loopable.AddLoopAction(LoopContext.Loop, checkSpeedIncrease, 1000);
			_loopable.AddLoopAction(LoopContext.Loop, checkAdvance, 1000);
		}

		public void Dispose()
		{
			_loopable.RemoveLoopAction(LoopContext.Loop, checkSpeedIncrease);
			_loopable.RemoveLoopAction(LoopContext.Loop, checkAdvance);
		}

		private void checkAdvance()
		{
			Time.AppTime time = _timeProvider.Get();
			double delta = _deltaTime + time.FrameDurationInSeconds;
			double deltaBorder = 1 / Speed;
			if (delta < deltaBorder)
			{
				_deltaTime = delta;
				return;
			}
			_deltaTime = delta - deltaBorder;
			advance();
		}

		private void advance()
		{
			FieldLine lineToCompare = _nextRouteSection.Lines[_routeSectionLineIndex];
			for (int i = 0; i<_field.Height; i++ )
			{
				FieldLine formerLine = new FieldLine(_field.Lines[i]);
				_field.UpdateFieldLine(i, lineToCompare);
				bool isSame = lineToCompare == formerLine;
				lineToCompare = formerLine;
			}
			_routeSectionLineIndex++;
			if (_routeSectionLineIndex >= _nextRouteSection.Height)
				chooseNextRootSection();
			OnAdvance?.Invoke();
		}

		private void chooseNextRootSection()
		{
			Random ran = new Random();
			int index = ran.Next(_possibleSections.Length);
			setNextRouteSection(_possibleSections[index]);
		}

		private void setNextRouteSection(Field routeSection)
		{
			_nextRouteSection = routeSection;
			_routeSectionLineIndex = 0;
		}

		private void checkSpeedIncrease()
		{
			Time.AppTime time = _timeProvider.Get();
			double delta = _deltaTimeSpeedIncrease + time.FrameDurationInSeconds;
			if (delta < _timeUnitDuration)
			{
				_deltaTimeSpeedIncrease = delta;
				return;
			}
			_deltaTimeSpeedIncrease = delta - _timeUnitDuration;
			_timeUnits++;
		}
	}
}
