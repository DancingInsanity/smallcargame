﻿using System;

namespace SmallCarGame.Time
{
	public class AppTimeCalculatorImpl : AppTimeCalculator
	{
		private DateTime _timeOfFormerCalculation;
		private DateTime _currentTime;
		private double _frameDuration;

		public AppTimeCalculatorImpl() : base()
		{
			_currentTime = DateTime.Now;
			_timeOfFormerCalculation = _currentTime;
		}

		protected override void calculateFrameDuration()
		{
			_currentTime = DateTime.Now;
			_frameDuration = (_currentTime - _timeOfFormerCalculation).TotalSeconds;
			_timeOfFormerCalculation = _currentTime;
		}

		protected override double getFrameDuration()
		{
			return _frameDuration;
		}
	}
}