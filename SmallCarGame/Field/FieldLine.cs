﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallCarGame
{
	class FieldLine
	{
		public List<FieldUnit> Units { get; }
		public int Length { get { return Units.Count; } }

		public FieldLine(List<FieldUnit> units)
		{
			Units = units;
		}

		public FieldLine(FieldLine copy)
		{
			List<FieldUnit> newUnits = new List<FieldUnit>();
			foreach (FieldUnit unit in copy.Units)
				newUnits.Add(new FieldUnit(unit));
			Units = new List<FieldUnit>(newUnits);
		}

		public void UpdateFieldUnit(int index, FieldUnit unit)
		{
			FieldUnit lineUnit = Units[index];
			if(lineUnit.IsAccessable != unit.IsAccessable)
				lineUnit.IsAccessable = unit.IsAccessable;
		}

		public void AddUnit(FieldUnit unit)
		{
			Units.Add(unit);
		}

		public void RemoveLastUnit()
		{
			Units.RemoveAt(Length - 1);
		}
	}
}
