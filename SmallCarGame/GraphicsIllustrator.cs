﻿using SmallCarGame.Core;
using System;

namespace SmallCarGame
{
	class GraphicsIllustrator: Initializable, IDisposable
	{
		private const char _accessableFieldUnit = '.';
		private const char _notAccessableFieldUnit = '#';
		private const char _playerUnit = '0';

		private readonly Field _field;
		private readonly Loopable _loopable;
		private readonly Game _game;

		public GraphicsIllustrator(Field field,
			Loopable loopable,
			Game game)
		{
			_field = field;
			_loopable = loopable;
			_game = game;
		}

		
		public void Initialize()
		{
			Console.CursorVisible = false;
			redraw();
			_loopable.AddLoopAction(LoopContext.FixedLoop, redraw, -100);
		}

		public void Dispose()
		{
			if(!_game.IsLost)
				_loopable.RemoveLoopAction(LoopContext.FixedLoop, redraw);
		}


		private void redraw()
		{
			if(_game.IsLost)
			{
				drawGameOverScreen();
				_loopable.RemoveLoopAction(LoopContext.FixedLoop, redraw);
				return;
			}

			//Console.Clear();
			Console.SetCursorPosition(0, 0);
			int width = _field.Width;
			foreach(FieldLine line in _field.Lines)
			{
				redrawLine(line, width);
			}
			drawPoints();
		}

		private void redrawLine(FieldLine line, int width)
		{
			int numOfSpaces = width - line.Length;
			int numOfPrevSpaces = numOfSpaces / 2;
			int numOfSubsSpaces = numOfSpaces - numOfPrevSpaces;
			for (int i = 0; i < width; i++)
			{
				if (i < numOfPrevSpaces || i >= line.Length + numOfPrevSpaces)
					drawUnit(null);
				else
					drawUnit(line.Units[i - numOfPrevSpaces]);
			}
			Console.WriteLine("");
		}


		private void drawUnit(FieldUnit unit)
		{
			Console.Write(getUnitChar(unit));
		}

		private char getUnitChar(FieldUnit unit)
		{
			if (unit == null)
				return ' ';
			if (unit.IsPlayerUnit)
				return _playerUnit;
			return unit.IsAccessable ? _accessableFieldUnit : _notAccessableFieldUnit;
		}

		private void drawPoints()
		{
			Console.WriteLine();
			Console.WriteLine($"----- Points: {_game.Points} -----");
		}

		private void drawGameOverScreen()
		{
			Console.Clear();
			Console.WriteLine("Congrats!");
			Console.WriteLine($"You gained {_game.Points} Points!");
			Console.WriteLine();
			Console.WriteLine("Press ESC to exit!");
		}
	}
}
