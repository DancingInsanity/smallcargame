﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallCarGame
{
	class PointsAdder: Initializable, IDisposable
	{
		private const int _pointsPerAdvance = 1;


		private Game _game;
		private RouteAdvancer _routeAdvancer;

		public PointsAdder(Game game,
			RouteAdvancer routeAdvancer)
		{
			_game = game;
			_routeAdvancer = routeAdvancer;
		}

		public void Initialize()
		{
			_routeAdvancer.OnAdvance += addPoints;
		}

		public void Dispose()
		{
			_routeAdvancer.OnAdvance -= addPoints;
		}

		private void addPoints()
		{
			_game.Points += _pointsPerAdvance;
		}
	}
}
