using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmallCarGame.Time;
using System;
using System.Threading;

namespace SmallCarGame.UnitTests
{
	[TestClass]
	public class AppTimeCalculatorImplTest
	{
		[TestMethod]
		public void Calculate_DoubleCall_CorrectCalculation()
		{
			// Arrange
			AppTimeCalculator appTimeCalculator = new AppTimeCalculatorImpl();
			int threadSleepTime = 100;
			int millisecondsWithinSeconds = 1000;
			double timeToleranceFactor = 0.01;
			double toleranceFrame = ((double)threadSleepTime / millisecondsWithinSeconds) * timeToleranceFactor;

			// Act 
			appTimeCalculator.Calculate();
			DateTime dateTimeAfterFirstCalculation = DateTime.Now;
			Thread.Sleep(threadSleepTime);
			appTimeCalculator.Calculate();
			AppTime appTime = appTimeCalculator.Get();
			DateTime dateTimeAfterSecondCalculation = DateTime.Now;
			double actualDelta = (dateTimeAfterSecondCalculation - dateTimeAfterFirstCalculation).TotalSeconds;

			// Assert
			Assert.AreEqual(actualDelta, appTime.FrameDurationInSeconds, toleranceFrame, $"Actual delta seconds {actualDelta} |" +
				$" Calculated delta seconds {appTime.FrameDurationInSeconds}. " +
				$"The calculated delta time isn't within the tolerance frame {toleranceFrame}. " +
				$"The difference between the actual and the calculated value is {Math.Abs(actualDelta - appTime.FrameDurationInSeconds)}");
		}

		[TestMethod]
		public void GetTime_DoubleCall_ValuesEqual()
		{
			// Arrange
			AppTimeCalculator appTimeCalculator = new AppTimeCalculatorImpl();

			// Act 
			appTimeCalculator.Calculate();
			AppTime firstAppTime = appTimeCalculator.Get();
			AppTime secondAppTime = appTimeCalculator.Get();

			// Assert
			Assert.AreEqual(firstAppTime, secondAppTime, $"Received two differnt AppTime values after calling {nameof(AppTimeCalculator.Get)}." +
				$"First value: {firstAppTime} | Second value: {secondAppTime}");
		}

		[TestMethod]
		public void GetTime_Call_DeltaTimeNotNegative()
		{
			// Arrange
			AppTimeCalculator appTimeCalculator = new AppTimeCalculatorImpl();

			// Act 
			appTimeCalculator.Calculate();
			AppTime appTime = appTimeCalculator.Get();

			// Assert
			Assert.IsTrue(appTime.FrameDurationInSeconds >= 0, $"The {nameof(AppTime)}'s {nameof(AppTime.FrameDurationInSeconds)} " +
				$"value is below zero: {appTime.FrameDurationInSeconds}");
		}
	}
}
