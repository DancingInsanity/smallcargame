﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmallCarGame.Time;
using SmallCarGame.Core;
using System;

namespace SmallCarGame.UnitTests
{
	[TestClass]
	public class LooperTest
	{
		private const int _numberOfCallsWithinOneFrame = 1;
		private const int _fixedFramesPerSecond = 60;
		private const double _timeToleranceFrame = 0;


		[TestMethod]
		public void AddLoopAction_OneLoopAction_LoopActionCalled()
		{
			AppTimeCalculator appTimeCalculator = new DummyAppTimeCalculator();
			Looper looper = new Looper(appTimeCalculator);
			Counter counter = new Counter();

			Action terminationAction = () => looper.StopLoop();
			int counterMax = 1;

			Action countAction = () => countUp(counter, counterMax, terminationAction);
			looper.AddLoopAction(LoopContext.Loop, countAction);
			looper.Initialize();
			appTimeCalculator.Calculate();
			looper.StartLoop();
			Assert.AreEqual(counter.Count, _numberOfCallsWithinOneFrame, $"The {nameof(countUp)} method should " +
				$"have been called {_numberOfCallsWithinOneFrame} times. But it was called {counter.Count} times.");
			looper.RemoveLoopAction(LoopContext.Loop, countAction);
		}

		[TestMethod]
		public void AddLoopAction_OneFixedLoopAction_CalledCountCorrect()
		{
			AppTimeCalculator appTimeCalculator = new DummyAppTimeCalculator();
			Looper looper = new Looper(appTimeCalculator, _fixedFramesPerSecond);
			Counter fixedLoopCounter = new Counter();
			Counter loopCounter = new Counter();

			Action terminationAction = () => looper.StopLoop();
			int counterMax = 1;

			Action fixedLoopcountAction = () => countUp(fixedLoopCounter, counterMax, terminationAction);
			Action loopAction = () => countUp(loopCounter, counterMax);
			looper.AddLoopAction(LoopContext.FixedLoop, fixedLoopcountAction);
			looper.AddLoopAction(LoopContext.Loop, loopAction);
			looper.Initialize();
			appTimeCalculator.Calculate();
			looper.StartLoop();
			int expectedNumberOfCalls = (int) (_fixedFramesPerSecond * DummyAppTimeCalculator.TimeDifference);
			Assert.AreEqual(expectedNumberOfCalls, fixedLoopCounter.Count, $"The {nameof(countUp)} method should " +
				$"have been called {expectedNumberOfCalls} times within a second. The actual number of calls was {fixedLoopCounter.Count}.");
			looper.RemoveLoopAction(LoopContext.Loop, fixedLoopcountAction);
			looper.RemoveLoopAction(LoopContext.Loop, loopAction);
		}

		private void countUp(Counter counter, int countMax, Action countReachedAction = null)
		{
			counter.IncreaseCounter();
			if (counter.Count == countMax)
				countReachedAction?.Invoke();
		}

		private class Counter
		{
			public int Count { get; protected set; } = 0;

			public void IncreaseCounter()
			{
				Count++;
			}

			public override string ToString()
			{
				return $"Counter: {Count}";
			}
		}
	}
}
