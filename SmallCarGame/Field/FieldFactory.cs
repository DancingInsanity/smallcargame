﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallCarGame
{
	class FieldFactory
	{
		private const char _notAccessableChar = '-';
		private const char _accessableChar = '#';

		public Field Create(char[][] template)
		{
			List<FieldLine> lines = new List<FieldLine>();
			foreach(char[] line in template)
			{
				lines.Add(createFieldLine(line));
			}
			return new Field(lines);
		}

		private FieldLine createFieldLine(char[] template)
		{
			List<FieldUnit> units = new List<FieldUnit>();
			foreach (char unit in template)
			{
				units.Add(createFieldUnit(unit));
			}
			return new FieldLine(units);
		}

		private FieldUnit createFieldUnit(char template)
		{
			if (template == _accessableChar)
				return new FieldUnit(true, false);
			if (template == _notAccessableChar)
				return new FieldUnit(false, false);
			return new FieldUnit();
		}
	}
}
