﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallCarGame.Collections
{
	class PrioritizedList<T>
	{
		public List<ListElement> List { get; private set; } = new List<ListElement>();

		public void Add(int priority, T value)
		{
			List.Add(new ListElement(priority, value));
			sortList();
		}

		public void Remove(T value)
		{
			IEnumerable<ListElement> elemtensToRemove = List.Where(e => e.Value.Equals(value));
			foreach (ListElement element in elemtensToRemove.ToList())
				List.Remove(element);
			sortList();
		}

		private void sortList()
		{
			List = List.OrderByDescending(e => e.Priority).ToList();
		}

		public struct ListElement
		{
			public int Priority { get; }
			public T Value { get; }

			public ListElement(int priority, T value)
			{
				Priority = priority;
				Value = value;
			}

			public override int GetHashCode()
			{
				return base.GetHashCode() * Priority.GetHashCode() * Value.GetHashCode();
			}
		}
	}
}
