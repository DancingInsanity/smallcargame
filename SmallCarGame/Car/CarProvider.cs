﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallCarGame
{
	interface CarProvider
	{
		Car GetCar();
	}
}
